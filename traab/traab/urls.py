from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from socketio import sdjango

from game.views import LoginView, RegisterView, RoomsView, CreateRoomView, \
	StartGameView, GameView
from utils.decorators import redirect_authenticated, redirect_incomplete


sdjango.autodiscover()


urlpatterns = patterns('game.views',
    url(r'^socket\.io', include(sdjango.urls)),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^room/create/?$', login_required(CreateRoomView.as_view()), name='create_room'),
    url(r'^room/(?P<room>\d*)/start/?$', redirect_incomplete(login_required(StartGameView.as_view())), name='start_game'),
    url(r'^room/(?P<room>\d*)/?$', redirect_incomplete(login_required(GameView.as_view())), name='room'),
    url(r'^rooms/?$', login_required(RoomsView.as_view()), name='rooms'),
    
    url(r'^register/?$', redirect_authenticated(RegisterView.as_view()), name='register'),
    url(r'^logout/?$', 'logout', name='logout'),
    url(r'^$', redirect_authenticated(LoginView.as_view()), name='login'),
)
