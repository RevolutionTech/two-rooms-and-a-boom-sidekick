# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0001_initial'),
        ('player', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='winning_team',
            field=models.ForeignKey(blank=True, to='player.Team', null=True),
            preserve_default=True,
        ),
    ]
