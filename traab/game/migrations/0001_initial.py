# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_players', models.IntegerField()),
                ('num_rounds', models.IntegerField(default=3)),
                ('created_datetime', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('started_datetime', models.DateTimeField(null=True, blank=True)),
                ('completed_datetime', models.DateTimeField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=20, db_index=True)),
                ('description', models.CharField(max_length=100, null=True, blank=True)),
                ('created_datetime', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('host', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num', models.IntegerField()),
                ('started_datetime', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('completed_datetime', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('game', models.ForeignKey(to='game.Game')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='round',
            unique_together=set([('game', 'num')]),
        ),
        migrations.AddField(
            model_name='game',
            name='room',
            field=models.ForeignKey(to='game.Room'),
            preserve_default=True,
        ),
    ]
