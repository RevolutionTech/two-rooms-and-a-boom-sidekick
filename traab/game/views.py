from django.contrib.auth import authenticate, \
	login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from game.forms import LoginForm, RegisterForm, RoomForm, CreateGameForm
from game.models import Room, Game
from game.sockets import RoomNamespace, _connections
from player.models import Team, Character, Player, Role


class LoginView(FormView):

	template_name = 'login.html'
	form_class = LoginForm

	def dispatch(self, request):
		self.success_url = request.GET.get('next', reverse('rooms'))
		return super(LoginView, self).dispatch(request)

	def get_context_data(self, **kwargs):
		context = super(LoginView, self).get_context_data(**kwargs)
		context['redirect_url'] = self.success_url
		return context

	def form_valid(self, form):
		d = form.cleaned_data
		username, password = d['username'], d['password']
		user = authenticate(username=username, password=password)
		if user:
			auth_login(self.request, user)
			return super(LoginView, self).form_valid(form)


class RegisterView(FormView):

	template_name = 'register.html'
	form_class = RegisterForm

	def dispatch(self, request):
		self.success_url = reverse('rooms')
		return super(RegisterView, self).dispatch(request)

	def form_valid(self, form):
		d = form.cleaned_data
		username, password = d['username'], d['password']
		del d['password_confirm']

		User.objects.create_user(**d)
		user = authenticate(username=username, password=password)
		auth_login(self.request, user)
		return super(RegisterView, self).form_valid(form)


def logout(request):
	auth_logout(request)
	return HttpResponseRedirect(reverse('login'))


class RoomsView(TemplateView):

	template_name = 'rooms.html'

	def get_context_data(self, **kwargs):
		context = super(RoomsView, self).get_context_data(**kwargs)
		context['rooms'] = map(
			lambda room: room.add_status(user=self.request.user),
			Room.objects.all()
		)
		return context


class CreateRoomView(FormView):

	template_name = 'create_room.html'
	form_class = RoomForm

	def dispatch(self, request):
		self.success_url = reverse('rooms')
		return super(CreateRoomView, self).dispatch(request)

	def form_valid(self, form):
		d = form.cleaned_data
		Room.objects.create(host=self.request.user, **d)
		return super(FormView, self).form_valid(form)


class StartGameView(FormView):

	template_name = 'start_game.html'
	form_class = CreateGameForm

	def add_nums_to_characters(self, characters):
		characters_with_nums = []
		for character in characters:
			character.num = self.request.POST.get('num_char_%d' % character.id, 0)
			characters_with_nums.append(character)
		return characters_with_nums

	def dispatch(self, request, room):
		self.success_url = reverse('room', args=[room,])

		# If a game is already active in this room, go to it
		if Room.objects.get(id=room).active_game():
			return HttpResponseRedirect(self.success_url)

		# If the user is not the host, kick them to the rooms screen
		if request.user != Room.objects.get(id=room).host:
			return HttpResponseRedirect(reverse('rooms'))

		return super(StartGameView, self).dispatch(request, room)

	def get_context_data(self, **kwargs):
		context = super(StartGameView, self).get_context_data(**kwargs)
		context['room'] = self.args[0]

		teams = []
		for team in Team.objects.all():
			characters = Character.objects.filter(team=team).order_by('id')
			teams.append({
				'name': team.name,
				'description': team.description,
				'characters': self.add_nums_to_characters(characters),
			})
		context['teams'] = teams
		
		return context

	def form_valid(self, form):
		d = form.cleaned_data
		room = Room.objects.get(id=self.args[0])
		characters = d['characters']
		max_players = sum(d['teams'].itervalues())

		# Create the game
		game = Game.objects.create(room=room, max_players=max_players)

		# Create roles for the game
		for character_id, character_num in characters.iteritems():
			character = Character.objects.get(id=character_id)
			for i in range(character_num):
				Role.objects.create(
					game=game,
					character=character
				)

		# Add the host to the game
		Player.objects.create(game=game, user=self.request.user)

		return super(StartGameView, self).form_valid(form)


class GameView(TemplateView):

	template_name = 'game.html'

	def dispatch(self, request, room):
		# Kick the user out of the room on any of the following conditions:
		# 1) There is no active game in the room
		# 2) The room is full and the user is not part of the game
		
		# There is no active game
		self.game = Room.objects.get(id=room).active_game()
		if not self.game:
			return HttpResponseRedirect(reverse('rooms'))
		
		# The room is full
		if self.game.num_players == self.game.max_players and \
			not Player.objects.filter(game=self.game, user=request.user).exists():
			return HttpResponseRedirect(reverse('rooms'))

		# If the user is not part of the game, they should join
		self.player, created = Player.objects.get_or_create(game=self.game, user=request.user)

		# Broadcast to all players in the room that the player has joined
		if created:
			for connection_id, connection in _connections.iteritems():
				connection.emit('player_join', self.player.display_name())

		return super(GameView, self).dispatch(request, room)
	
	def get_context_data(self, **kwargs):
		context = super(GameView, self).get_context_data(**kwargs)

		# Determine what characters are in the team
		teams = []
		for team in Team.objects.all():
			teams.append(
				(
					team.name,
					Role.objects.filter(
						game=self.game,
						character__team=team
					).order_by('character'),
				)
			)

		context.update({
			'teams': teams,
			'max_players': self.game.max_players,
			'players': map(
				lambda player: player.display_name(requesting_user=self.player.user),
				Player.objects.filter(game=self.game)
			),
		})
		return context
