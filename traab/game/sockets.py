from socketio.namespace import BaseNamespace
from socketio.mixins import BroadcastMixin, RoomsMixin
from socketio.sdjango import namespace


_connections = {}


@namespace("/game")
class RoomNamespace(BaseNamespace, RoomsMixin, BroadcastMixin):

	def initialize(self, *args, **kwargs):
		_connections[id(self)] = self
		super(RoomNamespace, self).initialize(*args, **kwargs)

	def disconnect(self, *args, **kwargs):
		del _connections[id(self)]
		super(RoomNamespace, self).disconnect(*args, **kwargs)

	def recv_disconnect(self):
		self.disconnect(silent=True)
		return True
