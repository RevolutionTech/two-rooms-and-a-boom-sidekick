from django import forms
from django.contrib.auth.models import User
from django.core.validators import validate_email

from game.models import Room, Game
from player.models import Team, Character


class LoginForm(forms.Form):

	FAILED_AUTH_WARNING = "The username/email and password do not match our records."

	username = forms.CharField(max_length=75)
	password = forms.CharField()

	@classmethod
	def user_from_email_or_username(cls, email_or_username):
		""" Get user from email/username """

		email = username = None
		try:
			validate_email(email_or_username)
			email = email_or_username
		except forms.ValidationError:
			username = email_or_username

		if email:
			try:
				return User.objects.get(email=email)
			except User.DoesNotExist:
				raise forms.ValidationError(cls.FAILED_AUTH_WARNING)

		elif username:
			try:
				return User.objects.get(username=username)
			except User.DoesNotExist:
				raise forms.ValidationError(cls.FAILED_AUTH_WARNING)

		else:
			raise forms.ValidationError(cls.FAILED_AUTH_WARNING)

	def clean(self):
		""" Verify that user with given credentials exists """

		cleaned_data = super(LoginForm, self).clean()

		if not self.errors:
			email_or_username = cleaned_data.get('username')
			password = cleaned_data.get('password')
			user = self.user_from_email_or_username(email_or_username)
			if user and user.check_password(password):
				cleaned_data['email'] = user.email
				cleaned_data['username'] = user.username
			else:
				raise forms.ValidationError(self.FAILED_AUTH_WARNING)

		return cleaned_data


class RegisterForm(forms.Form):

	username = forms.CharField(max_length=30)
	email = forms.EmailField(required=False, max_length=75)
	first_name = forms.CharField(required=False, max_length=30)
	last_name = forms.CharField(required=False, max_length=30)
	password = forms.CharField(min_length=5)
	password_confirm = forms.CharField(min_length=5)

	def clean_register_username(self):
		""" Verify that the username is not already taken """

		username = self.cleaned_data['username']
		if User.objects.filter(username=username).exists():
			raise forms.ValidationError("Sorry! An account for the username already exists.")
		return username

	def clean_register_email(self):
		""" Verify that the email is not already registered """

		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise forms.ValidationError("An account with this email address already exists.")
		return email

	def clean(self):
		"""
		Verify that user does not already exist and
		that password and password_confirm fields match
		"""

		cleaned_data = super(RegisterForm, self).clean()

		if not self.errors:
			password = cleaned_data.get('password')
			password_confirm = cleaned_data.get('password_confirm')

			if password != password_confirm:
				raise forms.ValidationError("Password and Password Confirm fields do not match.")

		return cleaned_data


class RoomForm(forms.ModelForm):

	class Meta:
		model = Room
		fields = ('name', 'description',)

	def clean_name(self):
		""" Verify that a room with the given name does not already exist """

		name = self.cleaned_data['name']
		if Room.objects.filter(name=name).exists():
			raise forms.ValidationError("A room with this name already exists.")
		return name


class CreateGameForm(forms.Form):

	def __init__(self, *args, **kwargs):
		super(CreateGameForm, self).__init__(*args, **kwargs)

		# Add fields dynamically- based on the characters in the db
		for character in Character.objects.exclude(name__in=Character.required_names):
			self.fields['num_char_{id}'.format(id=character.id)] = forms.IntegerField()
	
	def clean(self):
		""" Verify that the teams are of fair size """

		cleaned_data = super(CreateGameForm, self).clean()

		# Assign 1 to the required characters
		total_players = 0
		characters = {}
		teams = dict(map(lambda team: (team[0], 0), Team.objects.all().values_list('name')))
		for character in Character.objects.filter(name__in=Character.required_names):
			total_players += 1
			characters[character.id] = 1
			teams[character.team.name] += 1

		# Assign the value from the form to each remaining character
		for k, v in cleaned_data.iteritems():
			total_players += v
			character_id = int(k.split('num_char_')[1])
			characters[character_id] = v
			character = Character.objects.get(id=character_id)
			teams[character.team.name] += v

		# There should be at least 6 characters in play
		if total_players < 6:
			raise forms.ValidationError("At least 6 players are required to start a game.")

		# Blue and Red teams should be of equal size
		if teams['Red'] != teams['Blue']:
			raise forms.ValidationError("Red and Blue teams must be of equal size.")

		# There should be at most one Gambler
		gambler_id = Character.objects.get(name='Gambler').id
		if characters[gambler_id] > 1:
			raise forms.ValidationError("Only a single Gambler is allowed.")

		return {
			'teams': teams,
			'characters': characters,
		}
