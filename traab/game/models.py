from django.db import models
from django.contrib.auth.models import User

from player.models import Player


class Room(models.Model):

	name = models.CharField(max_length=20, unique=True, db_index=True)
	description = models.CharField(max_length=100, null=True, blank=True)
	host = models.ForeignKey(User)
	created_datetime = models.DateTimeField(auto_now_add=True, db_index=True)

	def __unicode__(self):
		return self.name

	def add_status(self, user):
		related_games = Game.objects.filter(room=self, completed_datetime__isnull=True)
		if related_games:
			related_game = related_games.get()
			self.num_players = related_game.num_players
			self.max_players = related_game.max_players
			self.num_rounds = related_game.num_rounds
			if not related_game.started_datetime:
				self.status = 'Available'
			else:
				self.status = 'In Progress'			
		else:
			if user == self.host:
				self.status = 'Idle'
			else:
				self.status = 'Unavailable'

		return self

	def active_game(self):
		try:
			return Game.objects.get(room=self, winning_team__isnull=True)
		except Game.DoesNotExist:
			return


class Game(models.Model):

	room = models.ForeignKey(Room, db_index=True)
	max_players = models.IntegerField()
	num_rounds = models.IntegerField(default=3)
	created_datetime = models.DateTimeField(auto_now_add=True, db_index=True)
	started_datetime = models.DateTimeField(null=True, blank=True)
	completed_datetime = models.DateTimeField(null=True, blank=True)
	winning_team = models.ForeignKey('player.Team', null=True, blank=True)

	def __unicode__(self):
		return self.room.name

	@property
	def num_players(self):
		return Player.objects.filter(game=self).count()


class Round(models.Model):
	
	game = models.ForeignKey(Game, db_index=True)
	num = models.IntegerField()
	started_datetime = models.DateTimeField(auto_now_add=True, db_index=True)
	completed_datetime = models.DateTimeField(null=True, blank=True, db_index=True)

	class Meta:
		unique_together = ('game', 'num',)

	def __unicode__(self):
		return "%s Round %s" % (self.game, self.num,)
