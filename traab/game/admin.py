from django.contrib import admin

from game.models import Room, Game, Round


# Register models 
for model in [Room, Game, Round]:
	admin.site.register(model)
