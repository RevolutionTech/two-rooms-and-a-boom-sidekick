# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


teams = (
    ('Blue', "Save the President!"),
    ('Red', "The President must be stopped!"),
    ('Grey', "Staying neutral in this main conflict."),
)
characters = (
    ('Blue', 'President', "If I die, the terrorists win.", 'img/blue_president.png'),
    ('Red', 'Bomber', "Peace was never an option.", 'img/red_bomber.png'),
    ('Blue', 'Blue Team', "You are number one.", 'img/blue_team.png'),
    ('Red', 'Red Team', "Live free or die hard.", 'img/red_team.png'),
    ('Grey', 'Gambler', "Place your bets.", 'img/grey_gambler.png'),
    ('Blue', 'Shy Guy', "I will not show you mine.", 'img/blue_shyguy.png'),
    ('Red', 'Shy Guy', "I've got castration issues.", 'img/red_shyguy.png'),
    ('Blue', 'Fool', "Ah... damn it.", 'img/blue_fool.png'),
    ('Red', 'Fool', "Derp.", 'img/red_fool.png'),
)


def create_teams(apps, schema_editor):
	Team = apps.get_model("player", "Team")
	for name, description in teams:
		Team.objects.create(name=name, description=description)


def remove_teams(apps, schema_editor):
	Team = apps.get_model("player", "Team")
	for name, description in teams:
		Team.objects.get(name=name).delete()


def create_characters(apps, schema_editor):
	Team = apps.get_model("player", "Team")
	Character = apps.get_model("player", "Character")
	for team_name, name, description, img in characters:
		team = Team.objects.get(name=team_name)
		Character.objects.create(team=team, name=name, description=description, img=img)


def remove_characters(apps, schema_editor):
	Team.apps.get_model("player", "Team")
	Character = apps.get_model("player", "Character")
	for team_name, name, description, img in characters:
		team = Team.objects.get(name=team_name)
		Characters.objects.get(team=team, name=name).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('player', '0001_initial'),
    ]

    operations = [
    	migrations.RunPython(create_teams, remove_teams),
    	migrations.RunPython(create_characters, remove_characters),
    ]
