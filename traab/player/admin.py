from django.contrib import admin

from player.models import Team, Character, Player, Role


# Register models 
for model in [Team, Character, Player, Role]:
	admin.site.register(model)
