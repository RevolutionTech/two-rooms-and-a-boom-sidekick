from django.db import models
from django.contrib.auth.models import User


class Team(models.Model):

	name = models.CharField(max_length=10, db_index=True)
	description = models.CharField(max_length=300, null=True, blank=True)

	def __unicode__(self):
		return self.name


class Character(models.Model):

	team = models.ForeignKey(Team, db_index=True)
	name = models.CharField(max_length=25, db_index=True)
	description = models.CharField(max_length=300, null=True, blank=True)
	img = models.ImageField(upload_to='img/')

	required_names = ['President', 'Bomber',]

	class Meta:
		unique_together = ('team', 'name',)

	def __unicode__(self):
		return "%s %s" % (self.team, self.name,)

	def required(self):
		return self.name in self.required_names


class Player(models.Model):

	game = models.ForeignKey('game.Game', db_index=True)
	user = models.ForeignKey(User, db_index=True)
	won = models.NullBooleanField(null=True, blank=True, db_index=True)

	class Meta:
		unique_together = ('game', 'user',)

	def __unicode__(self):
		return ' '.join([
			self.user.first_name,
			self.user.last_name,
			'[{}]'.format(self.user.username)
		])

	def display_name(self, requesting_user=None):
		display_note = ''
		if requesting_user and requesting_user == self.user:
			display_note = ' (you)'
		elif self.game.room.host == self.user:
			display_note = ' (host)'
		return ''.join([unicode(self), display_note])


class Role(models.Model):

	game = models.ForeignKey('game.Game', db_index=True)
	character = models.ForeignKey(Character, db_index=True)
	player = models.ForeignKey(Player, null=True, blank=True, db_index=True)

	def __unicode__(self):
		if self.player:
			return "%s in %s" % (self.player, self.game,)
		else:
			return "%s in %s" % (self.character, self.game)
