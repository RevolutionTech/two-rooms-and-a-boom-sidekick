import functools

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from game.models import Room


def redirect_authenticated(func):
	""" If the user is already authenticated, redirect to rooms """

	@functools.wraps(func)
	def wrapper(request, *args, **kwargs):
		if request.user.is_authenticated():
			return HttpResponseRedirect(reverse('rooms'))
		return func(request, *args, **kwargs)

	return wrapper


def redirect_incomplete(func):
	""" If the room is unspecified, redirect to rooms """

	@functools.wraps(func)
	def wrapper(request, room, *args, **kwargs):
		if not room or not Room.objects.filter(id=room).exists():
			return HttpResponseRedirect(reverse('rooms'))
		return func(request, room, *args, **kwargs)

	return wrapper
