$(document).ready(function (){
	'use strict'

	function pluralize (str, num) {
		if ( num == 1 ){
			return str;
		}
		else {
			return str + 's';
		}
	};

	function sum (array) {
		return array.reduce( function (a, b) {
			return a + b;
		});
	};

	function getNumPlayers () {
		var all_characters = $( '.num-character' );
		var players_per_char = all_characters.map( function (){
			return parseInt( $( this ).val() ) || 0;
		});
		return sum(players_per_char.toArray());
	};

	function updateStartBtn () {
		var min_players = 6;
		var num_players = getNumPlayers();
		var start_btn = $( '.start-btn' );
		if ( num_players < min_players ){
			var more_players_required = min_players - num_players;
			start_btn.prop( 'disabled', true );
			start_btn.html( 'Need ' + more_players_required + ' more ' + pluralize( 'player', more_players_required ) + ' to start a game with 6 players' );
		}
		else {
			start_btn.html( 'Start a game with ' + num_players + pluralize( ' player', num_players ) );
			start_btn.prop( 'disabled', false );
		}
	};

	var all_characters = $( '.num-character' );
	all_characters.change( updateStartBtn );
	all_characters.keyup( updateStartBtn );
});
