$(document).ready(function (){
	'use strict'

	/* Socket.io */

	var socket = io.connect( "/game" );

	socket.on( 'player_join', function (name) {
		player_join(name);
	});

	socket.on( 'connect', function () {
		console.log('Connected!');
	});

	socket.on( 'reconnect', function () {
		console.log('Reconnected!');
	});

	socket.on( 'reconnecting', function () {
		console.log('Attempting to reconnect to the server...');
	});

	socket.on( 'error', function (e) {
		console.log(e ? e : 'An unknown error occurred.');
	});

	/* Game functions */

	function update_players () {
		var player_list = players.map( function (name) {
			return '<li>' + name + '</li>';
		});
		$( '.player-list' ).html( player_list );
		if ( players.length < max_players ){
			var num_seats_left = max_players - players.length;
			$( '.header-link' ).html( 'Waiting for ' + num_seats_left + ' more players...' );
		}
		else {
			$( '.header-link' ).html( 'Waiting for host to start game...' );
		}
	};

	function player_join (name) {
		players.push( name );
		update_players();
	};

	update_players();
});
